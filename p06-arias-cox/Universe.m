//
//  Universe.m
//  p06-arias-cox
//
//  Created by Em on 4/10/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "Universe.h"

//may be scrapped if high score not needed

@implementation Universe
@synthesize counter;

static Universe *singleton = nil;

-(id)init{
    if(singleton){
        return singleton;
    }
    self = [super init];
    if(self){
        singleton = self;
    }
    return self;
}
+(Universe *)sharedInstance{
    if(singleton){
        return singleton;
    }
    return [[Universe alloc] init];
}

-(void)saveState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError * err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"scoreData.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeInt:counter forKey:@"counter"];
    [archiver finishEncoding];
    [data writeToURL:url atomically:YES];
    NSLog(@"Store score %d",counter);
}
-(void)loadState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError * err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"scoreData.archive" relativeToURL:[dirs objectAtIndex:0]];

    NSData *data = [NSData dataWithContentsOfURL:url];
    if(!data){
        return;
    }
    
    NSKeyedUnarchiver *unarchiver;
    unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    counter = [unarchiver decodeIntForKey:@"counter"];
    NSLog(@"Loaded score %d",counter);
}
@end
