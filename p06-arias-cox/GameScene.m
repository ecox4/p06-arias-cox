//
//  GameScene.m
//  p06-arias-cox
//
//  Created by Em on 4/11/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "GameScene.h"

@interface GameScene() <SKPhysicsContactDelegate>

@property BOOL contentCreated;
@property BOOL gameOver;

@property(nonatomic) int scoreP1;
@property(nonatomic) int scoreP2;
@property(nonatomic) int playerHit;

@end

@implementation GameScene{
    //spriteNode objs
    SKSpriteNode *paddle1;
    SKSpriteNode *paddle2;
    SKSpriteNode *ball;
    SKSpriteNode *bg;
    //danger blocks
    SKSpriteNode *fastBall;
    SKSpriteNode *minusPt;
    SKSpriteNode *smallerP;
    SKLabelNode *p1L;
    SKLabelNode *p2L;
    SKLabelNode *over;
}
-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
        self.gameOver = NO;
        self.scoreP1 = 0;
        self.scoreP2 = 0;
        self.playerHit = 0;
        self.physicsWorld.contactDelegate = self;
        self.backgroundColor = [SKColor whiteColor];
    }
}
-(void)createSceneContents{
    self.scaleMode = SKSceneScaleModeFill;
    //add spriteNodes to parent
//    [self addChild:[self setBg]];
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = paddle;
    [self addChild:[self setPaddleP1]];
    [self addChild:[self setPaddleP2]];
    [self addChild:[self setBall]];
    [self addChild:[self displayScoreP1]];
    [self addChild:[self displayScoreP2]];
}

-(void)update:(NSTimeInterval)currentTime{
    //check if ball touches y max or y min
    //ball.physicsBody.velocity = CGVectorMake(200,200);
    
    if(ball.physicsBody.velocity.dx != 0 && ball.physicsBody.velocity.dy != 0){
        float xdir;
        if(ball.physicsBody.velocity.dx < 0){
            xdir = -.05;
        }
        else{
            xdir = .05;
        }
        float ydir;
        if(ball.physicsBody.velocity.dy < 0){
            ydir = -.05;
        }
        else{
            ydir = .05;
        }
        [ball.physicsBody applyImpulse:CGVectorMake(xdir, ydir)];
        /*if(fabsf(ball.physicsBody.velocity.dx) < 10 && fabsf(ball.physicsBody.velocity.dy) < 10){
            
            [ball.physicsBody applyImpulse:CGVectorMake(xdir, ydir)];
        }*/
    }
    
    if(ball.position.y > CGRectGetMaxY(self.frame)-100){
        self.scoreP1++;
        if(self.playerHit == 0){
            if(fastBall)
                [fastBall removeFromParent];
            if(minusPt)
                [minusPt removeFromParent];
            if(smallerP)
                [smallerP removeFromParent];
        }else{
            [self doPenalty];
        }
        self.playerHit = 0;
        [ball removeFromParent];
        [self addChild:[self setBall]];
        [self doPenalty];
    }else if(ball.position.y < CGRectGetMinY(self.frame)+100){
        self.scoreP2++;
        if(self.playerHit == 0){
            if(fastBall)
                [fastBall removeFromParent];
            if(minusPt)
                [minusPt removeFromParent];
            if(smallerP)
                [smallerP removeFromParent];
        }else{
            [self doPenalty];
        }
        self.playerHit = 0;
        [ball removeFromParent];
        [self addChild:[self setBall]];
    }
    p1L.text = [NSString stringWithFormat:@"P1: %d",self.scoreP1];
    p2L.text = [NSString stringWithFormat:@"P2: %d",self.scoreP2];
    if((self.scoreP1 == 15)||(self.scoreP2 == 15)){
        //do game over
        [self addChild:[self doGameOver]];
        if(fastBall)
            [fastBall removeFromParent];
        if(minusPt)
            [minusPt removeFromParent];
        if(smallerP)
            [smallerP removeFromParent];
        NSLog(@"Game Over");
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if(ball.physicsBody.velocity.dx == 0 && ball.physicsBody.velocity.dy == 0){
        int rand = arc4random()%2;
        if(rand == 0){
            rand -= 1;
        }
        int randx = arc4random_uniform(4);
        int randy = arc4random_uniform(3) + 1;
        [ball.physicsBody applyImpulse:CGVectorMake(rand*randx, rand*randy)];
   }
    
    if(location.y < (CGRectGetMaxY(self.frame)/2)){
        SKPhysicsBody* tmpaddle2 = paddle2.physicsBody;
        paddle2.physicsBody = nil;
        paddle2.position = CGPointMake(location.x, paddle2.position.y);
        paddle2.physicsBody = tmpaddle2;
    }
    else{
        SKPhysicsBody* tmpaddle1 = paddle1.physicsBody;
        paddle1.physicsBody = nil;
        paddle1.position = CGPointMake(location.x, paddle1.position.y);
        paddle1.physicsBody = tmpaddle1;
    }
    SKNode *node =[self nodeAtPoint:location];
    if([node.name isEqualToString:@"gameover"]){
        [over removeFromParent];
        [self setBackgroundColor:[SKColor whiteColor]];
        self.scoreP1 = 0;
        self.scoreP2 = 0;
    }
}
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if(location.y < (CGRectGetMaxY(self.frame)/2)){
        SKPhysicsBody* tmpaddle2 = paddle2.physicsBody;
        paddle2.physicsBody = nil;
        paddle2.position = CGPointMake(location.x, paddle2.position.y);
        paddle2.physicsBody = tmpaddle2;
    }
    else{
        SKPhysicsBody* tmpaddle1 = paddle1.physicsBody;
        paddle1.physicsBody = nil;
        paddle1.position = CGPointMake(location.x, paddle1.position.y);
        paddle1.physicsBody = tmpaddle1;
    }
}
-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1, *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
        body1 = contact.bodyA;
        body2 = contact.bodyB;
    }else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
    }
    
    if(((body1.categoryBitMask & paddle) != 0)&&((body2.categoryBitMask & bouncer) != 0)){
        if(ball.position.y > CGRectGetMidY(self.frame)){
            self.playerHit = 2;
        }else{
            self.playerHit = 1;
        }
    }else if(((body1.categoryBitMask & bouncer) != 0)&&((body2.categoryBitMask & fireBlk) != 0)){
        //should only last for 30 sec
        paddle1.size = CGSizeMake(45, 10);
        paddle2.size = CGSizeMake(45, 10);
        SKNode *tmp = [self childNodeWithName:@"fire"];
        SKAction *remove = [SKAction removeFromParent];
        [tmp runAction:remove];
    }else if(((body1.categoryBitMask & bouncer) != 0)&&((body2.categoryBitMask & fastBlk) != 0)){
        
        SKNode *tmp = [self childNodeWithName:@"fast"];
        SKAction *remove = [SKAction removeFromParent];
        [tmp runAction:remove];
    }else if(((body1.categoryBitMask & bouncer) != 0)&&((body2.categoryBitMask & minusBlk) != 0)){
        //should only last for short time
        if(self.playerHit == 1){
            --self.scoreP1;
        }else if(self.playerHit == 2){
            --self.scoreP2;
        }
        self.playerHit = 0;
        SKNode *tmp = [self childNodeWithName:@"losePt"];
        SKAction *remove = [SKAction removeFromParent];
        [tmp runAction:remove];
    }
}
-(void)doPenalty{
    NSLog(@"do penalty");
    int penalty = arc4random()%3;
    CGFloat xPos = (CGFloat)(arc4random()%((int)self.frame.size.width));
    CGFloat yPos = (CGFloat)(arc4random()%((int)self.frame.size.height - 400)+200);
    CGPoint pt = CGPointMake(xPos, yPos);
    if(penalty == 0){
        [self addChild:[self addFastBlock:pt]];
    }else if(penalty == 1){
        [self addChild:[self addFireBlock:pt]];
    }else{
        [self addChild:[self addMinusBlock:pt]];
    }
}
-(SKLabelNode *)doGameOver{
    over = [SKLabelNode labelNodeWithFontNamed:@"Courier-Bold"];
    if(self.scoreP1 == 15){
        over.text = @"Player 1 Wins";
    }else{
        over.text = @"Player 2 Wins";
    }
    over.fontSize = 80;
    over.fontColor = [SKColor whiteColor];
    over.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    over.name = @"gameover";
    [self setBackgroundColor:[SKColor blackColor]];
    return over;
}
-(SKLabelNode *)displayScoreP1{
    p1L = [SKLabelNode labelNodeWithFontNamed:@"Courier-Bold"];
    p1L.text = [NSString stringWithFormat:@"P1: %d",self.scoreP1];
    p1L.fontSize = 28;
    p1L.fontColor = [SKColor grayColor];
    p1L.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+50);
    p1L.name = @"scoreKeeperP1";
    return p1L;
}
-(SKLabelNode *)displayScoreP2{
    p2L = [SKLabelNode labelNodeWithFontNamed:@"Courier-Bold"];
    p2L.text = [NSString stringWithFormat:@"P2: %d",self.scoreP2];
    p2L.fontSize = 28;
    p2L.fontColor = [SKColor grayColor];
    p2L.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)-50);
    p2L.name = @"scoreKeeperP2";
    return p2L;
}
-(SKSpriteNode *)addFireBlock:(CGPoint)pt{
    smallerP = [SKSpriteNode spriteNodeWithImageNamed:@"fire.png"];
    smallerP.size = CGSizeMake(60, 60);
    smallerP.position = pt;
    smallerP.physicsBody = [SKPhysicsBody bodyWithTexture:smallerP.texture size:smallerP.size];
    smallerP.physicsBody.affectedByGravity = NO;
    smallerP.physicsBody.contactTestBitMask = bouncer;
    smallerP.physicsBody.categoryBitMask = fireBlk;
    smallerP.physicsBody.collisionBitMask = 0;
    smallerP.name = @"fire";
    return smallerP;
}
-(SKSpriteNode *)addFastBlock:(CGPoint)pt{
    fastBall = [SKSpriteNode spriteNodeWithImageNamed:@"fast.png"];
    fastBall.size = CGSizeMake(60, 60);
    fastBall.position = pt;
    fastBall.physicsBody = [SKPhysicsBody bodyWithTexture:fastBall.texture size:fastBall.size];
    fastBall.physicsBody.affectedByGravity = NO;
    fastBall.physicsBody.contactTestBitMask = bouncer;
    fastBall.physicsBody.categoryBitMask = fastBlk;
    fastBall.physicsBody.collisionBitMask = 0;
    fastBall.name = @"fast";
    return fastBall;
}
-(SKSpriteNode *)addMinusBlock:(CGPoint)pt{
    minusPt = [SKSpriteNode spriteNodeWithImageNamed:@"lose_point.png"];
    minusPt.size = CGSizeMake(60, 60);
    minusPt.position = pt;
    minusPt.physicsBody = [SKPhysicsBody bodyWithTexture:minusPt.texture size:minusPt.size];
    minusPt.physicsBody.affectedByGravity = NO;
    minusPt.physicsBody.contactTestBitMask = bouncer;
    minusPt.physicsBody.categoryBitMask = minusBlk;
    minusPt.physicsBody.collisionBitMask = 0;
    minusPt.name = @"losePt";
    return minusPt;
}

-(SKSpriteNode *)setPaddleP1{
    paddle1 = [SKSpriteNode spriteNodeWithColor:[SKColor grayColor] size:CGSizeMake(75, 10)];
    paddle1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(75, 10)];
    paddle1.physicsBody.contactTestBitMask = bouncer;
    paddle1.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)-100);
    paddle1.physicsBody.affectedByGravity = NO;
    paddle1.physicsBody.dynamic = NO;
    paddle1.physicsBody.allowsRotation = NO;
    paddle1.physicsBody.categoryBitMask = paddle;
    paddle1.name = @"paddle1";
    return paddle1;
}
-(SKSpriteNode *)setPaddleP2{
    paddle2 = [SKSpriteNode spriteNodeWithColor:[SKColor grayColor] size:CGSizeMake(75, 10)];
    paddle2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(75, 10)];
    paddle2.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+100);
    paddle2.physicsBody.affectedByGravity = NO;
    paddle2.physicsBody.dynamic = NO;
    paddle2.physicsBody.allowsRotation = NO;
    paddle2.physicsBody.contactTestBitMask = bouncer;
    paddle2.physicsBody.categoryBitMask = paddle;
    paddle2.name = @"paddle2";
    return paddle2;
}
-(SKSpriteNode *)setBall{
    ball = [SKSpriteNode spriteNodeWithImageNamed:@"ball.png"];
    ball.size = CGSizeMake(20, 20);
    ball.physicsBody = [SKPhysicsBody bodyWithTexture:ball.texture size:ball.size];
    ball.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    ball.physicsBody.affectedByGravity = NO;
    ball.physicsBody.dynamic = YES;
    ball.physicsBody.allowsRotation = YES;
    ball.physicsBody.categoryBitMask = bouncer;
    ball.physicsBody.usesPreciseCollisionDetection = YES;
    ball.name = @"ball";
    return ball;
}
-(SKSpriteNode *)setBg{
    bg.name = @"bg";
    return bg;
}
@end
