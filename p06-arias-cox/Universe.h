//
//  Universe.h
//  p06-arias-cox
//
//  Created by Em on 4/10/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Universe : NSObject

+(Universe *)sharedInstance;
@property (nonatomic) int counter;

-(void)saveState;
-(void)loadState;

@end
