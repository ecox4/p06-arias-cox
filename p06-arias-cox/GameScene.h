//
//  GameScene.h
//  p06-arias-cox
//
//  Created by Em on 4/11/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene

@end

static const uint32_t paddle = 0x1 <<1;
static const uint32_t bouncer = 0x1 <<2;
//danger collision, no bounce should occur
static const uint32_t fireBlk = 0x1 <<3;
static const uint32_t fastBlk = 0x1 <<4;
static const uint32_t minusBlk = 0x1 <<5;
