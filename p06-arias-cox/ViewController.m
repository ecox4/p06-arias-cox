//
//  ViewController.m
//  p06-arias-cox
//
//  Created by Em on 4/7/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "ViewController.h"
#import "GameScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SKView *spriteView = (SKView *)self.view;
    spriteView.showsFPS = YES;
    spriteView.showsPhysics = YES;
    spriteView.showsNodeCount =YES;
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated{
    GameScene * game = [[GameScene alloc]initWithSize:CGSizeMake(800, 1200)];
    SKView *spriteView = (SKView *)self.view;
    [spriteView presentScene:game];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
